package com.ukrim.ppbl.posapp.ui.product

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ukrim.ppbl.posapp.R
import com.ukrim.ppbl.posapp.model.BukuData


class ProductAdapter(
    private val books: MutableList<BukuData>,
    private val listener: Listener
) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindModel(books.get(position), listener)
    }

    interface Listener {
        fun onItemClick(product: BukuData)
        fun onItemLongClick(product: BukuData)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvNamaProduct: TextView = itemView.findViewById(R.id.tv_judul)
        val tvHargaProduct: TextView = itemView.findViewById(R.id.tv_harga)
        val tvStockProduct: TextView = itemView.findViewById(R.id.tv_stock)
        val ivImageProduct: ImageView = itemView.findViewById(R.id.iv_product)
        fun bindModel(product : BukuData, listener: Listener) {
            tvNamaProduct.text = product.judulBuku
            tvHargaProduct.text = product.harga
            tvStockProduct.text = product.stock
            Glide.with(itemView.context).load(product.imageUrl).into(ivImageProduct)
            itemView.setOnClickListener {
                listener.onItemClick(product)
            }
            itemView.setOnLongClickListener {
                listener.onItemLongClick(product)
                true
            }
        }
    }
}