package com.ukrim.ppbl.posapp.presenter

import android.content.Context
import com.ukrim.ppbl.posapp.model.BukuData
import com.ukrim.ppbl.posapp.networking.RetrofitFactory
import com.ukrim.ppbl.posapp.view.CommonView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AllPresenter(
    private val view: CommonView,
    private val context: Context
) {
    fun getToken(auth: String) {
        view.showLoading()
        val api = RetrofitFactory.create()
        val request = api.getToken(auth)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
    fun getAllBuku() {
        view.showLoading()
        val api = RetrofitFactory.create(context)
        val request = api.getAllBuku()
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
    fun createBuku(buku : BukuData){
        view.showLoading()
        val api = RetrofitFactory.create(context)
        val request = api.createBuku(
            buku.gambar,
            buku.judulBuku,
            buku.penciptaBuku,
            buku.description,
            buku.harga,
            buku.stock
        )
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
    fun deleteBuku(id : String){
        view.showLoading()
        val api = RetrofitFactory.create(context)
        val request = api.deleteBuku(id)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
    fun updateBuku(buku : BukuData){
        view.showLoading()
        val api = RetrofitFactory.create(context)
        val request = api.updateBuku(buku)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }

}