package com.ukrim.ppbl.posapp.networking

import com.ukrim.ppbl.posapp.model.LoginResponse
import com.ukrim.ppbl.posapp.model.BooksResponse
import com.ukrim.ppbl.posapp.model.BukuData
import com.ukrim.ppbl.posapp.model.SuccessResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.*


interface RetrofitService {
    @POST("log")
    fun getToken(
        @Header("Authorization") basic: String
    ): Deferred<LoginResponse>

    @GET("buku")
    fun getAllBuku(): Deferred<BooksResponse>

    @FormUrlEncoded
    @POST("buku")
    fun createBuku(
        @Field("gambar") gambar : String,
        @Field("judul_buku") judulBuku : String,
        @Field("pencipta_buku") penciptaBuku : String,
        @Field("description") description : String,
        @Field("harga") harga : String,
        @Field("stock") stock : String
    ): Deferred<SuccessResponse>
    @DELETE("buku/{id}")
    fun deleteBuku(@Path("id") idBuku : String) : Deferred<SuccessResponse>

    @PUT("buku")
    fun updateBuku(@Body produk : BukuData) : Deferred<SuccessResponse>
}