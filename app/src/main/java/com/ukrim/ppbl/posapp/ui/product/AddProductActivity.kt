package com.ukrim.ppbl.posapp.ui.product

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import com.bumptech.glide.Glide
import com.tfb.fbtoast.FBToast
import com.ukrim.ppbl.posapp.R
import com.ukrim.ppbl.posapp.model.BukuData
import com.ukrim.ppbl.posapp.presenter.AllPresenter
import com.ukrim.ppbl.posapp.view.CommonView
import kotlinx.android.synthetic.main.activity_add_product.*
import java.io.ByteArrayOutputStream

class AddProductActivity : AppCompatActivity(), CommonView {
    lateinit var presenter: AllPresenter
    private var imageBase64: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)
        settingTombolSave();
    }

    private fun validate(): Boolean {
        var result = true
        if (imageBase64 == "" ||
            et_judul.text.toString() == "" ||
            et_pencipta.text.toString() == "" ||
            et_description.text.toString() == "" ||
            et_harga.text.toString() == "" ||
            et_harga.text.toString() == "0" ||
            et_stock.text.toString() == "" ||
            et_stock.text.toString() == "0"
        ) {
            result = false
        }
        return result
    }

    private fun settingTombolSave() {
        btn_save_product.setOnClickListener {
            val bukuData = BukuData(
                "0",
                imageBase64,
                et_judul.text.toString(),
                et_pencipta.text.toString(),
                et_description.text.toString(),
                et_harga.text.toString(),
                et_stock.text.toString(),
                "",
                "",
                ""
            );
            if(validate()) {
                btn_save_product.isEnabled = false
                presenter = AllPresenter(this, this)
                presenter.createBuku(bukuData)
            }else{
                FBToast.errorToast(this,"Ada buku yang masih salah",FBToast.LENGTH_SHORT)
            }
        }
        iv_produk.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, 12)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 12 && resultCode == Activity.RESULT_OK) {
            var bitmap = data?.extras?.get("data") as Bitmap
            val byteArrayOS = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOS)
            imageBase64 = Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT)
            Glide.with(this).load(bitmap).into(iv_produk);
        }
    }

    override fun success(anyResponse: Any) {
        FBToast.successToast(this, "Buku berhasil disimpan", FBToast.LENGTH_SHORT)
        imageBase64 = ""
        et_judul.setText("")
        et_pencipta.setText("")
        et_description.setText("")
        et_harga.setText("")
        et_stock.setText("")
        btn_save_product.isEnabled = true;
    }

    override fun error(error: Throwable) {
        btn_save_product.isEnabled = true;
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }
}
