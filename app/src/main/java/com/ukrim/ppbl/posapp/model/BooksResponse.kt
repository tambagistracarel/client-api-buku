package com.ukrim.ppbl.posapp.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class BooksResponse(
    @SerializedName("meta")
    val meta: Meta,
    @SerializedName("buku_data")
    val bukuData: MutableList<BukuData>
)

data class BukuData(
    @SerializedName("id_buku")
    val idBuku: String,
    @SerializedName("gambar")
    val gambar: String,
    @SerializedName("judul_buku")
    val judulBuku: String,
    @SerializedName("pencipta_buku")
    val penciptaBuku: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("harga")
    val harga: String,
    @SerializedName("stock")
    val stock: String,
    @SerializedName("image_url")
    val imageUrl: String,
    @SerializedName("create_at")
    val createAt: String,
    @SerializedName("update_at")
    val updateAt: String
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(idBuku)
        writeString(gambar)
        writeString(judulBuku)
        writeString(penciptaBuku)
        writeString(description)
        writeString(harga)
        writeString(stock)
        writeString(imageUrl)
        writeString(createAt)
        writeString(updateAt)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<BukuData> = object : Parcelable.Creator<BukuData> {
            override fun createFromParcel(source: Parcel): BukuData = BukuData(source)
            override fun newArray(size: Int): Array<BukuData?> = arrayOfNulls(size)
        }
    }
}