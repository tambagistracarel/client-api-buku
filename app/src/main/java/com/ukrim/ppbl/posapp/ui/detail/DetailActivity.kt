package com.ukrim.ppbl.posapp.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.ukrim.ppbl.posapp.R
import com.ukrim.ppbl.posapp.model.BukuData
import com.ukrim.ppbl.posapp.view.CommonView
import kotlinx.android.synthetic.main.activity_add_product.*
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.item_product.*

class DetailActivity : AppCompatActivity(),CommonView {

    lateinit var buku: BukuData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        buku = intent.getParcelableExtra("buku")
        populateData()
    }

    private fun populateData(){
        Glide.with(this).load(buku.imageUrl).into(iv_buku)
        t_judul.setText(buku.judulBuku)
        tv_pencipta.setText(buku.penciptaBuku)
        tv_description.setText(buku.description)
        t_harga.setText(buku.harga)
        t_stock.setText(buku.stock)
    }

    override fun showLoading() {

    }

    override fun error(error: Throwable) {

    }

    override fun success(anyResponse: Any) {

    }

    override fun hideLoading() {

    }
}
